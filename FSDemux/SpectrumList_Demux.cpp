//
// SpectrumList_Demux.cpp
//
// Original author: Jarrett Egertson <jegertso .@. uw.edu>
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
//

#define PWIZ_SOURCE

#include "SpectrumList_Demux.hpp"
#include "PrecursorMaskCodec.hpp"
#include "OverlapDemultiplexer.hpp"
#include "MSXDemultiplexer.hpp"
#include "SpectrumPeakExtractor.hpp"
#include <SpectrumListCache.hpp>
#include "pwiz/analysis/spectrum_processing/SpectrumListFactory.hpp"
#include <boost/make_shared.hpp>
#include <boost/algorithm/string.hpp>

#ifdef _PROFILE_PERFORMANCE
#include <chrono>
#include <iostream>
#endif

namespace pwiz {
namespace analysis {

	using namespace std;
	using namespace msdata;
#ifdef _PROFILE_PERFORMANCE
	using namespace std;
	using namespace chrono;
#endif

	inline MSXDemultiplexer::Params GenerateMSXParams(SpectrumList_Demux::Params p)
	{
		MSXDemultiplexer::Params msxParams;
		msxParams.applyWeighting = p.applyWeighting;
		msxParams.ppmError = p.ppmError;
		msxParams.variableFill = p.variableFill;
		return msxParams;
	}

	SpectrumList_Demux::IndexMapper::IndexMapper(SpectrumListPtr originalIn, const IPrecursorMaskCodec& pmc)
		: original(originalIn), detailLevel(DetailLevel::DetailLevel_InstantMetadata)
	{
		if (!original.get()) throw runtime_error("[SpectrumlList_MsxDemux] Null pointer");

		// iterate through the spectra, building the expanded index
		for (size_t i = 0, end = original->size(); i < end; ++i)
		{
			const auto& spectrumIdentity = original->spectrumIdentity(i);
			do
			{
				auto spectrum = original->spectrum(i, detailLevel);
				int msLevel = 0;
				if (TryGetMSLevel(*spectrum, msLevel))
				{
					pushSpectrum(spectrumIdentity, msLevel, pmc.GetPrecursorsPerSpectrum(), pmc.GetOverlapsPerCycle());
					break;
				}
				detailLevel = DetailLevel(int(detailLevel) + 1);
			} while (int(detailLevel) <= int(DetailLevel::DetailLevel_FullMetadata));
		}
	}

	void SpectrumList_Demux::IndexMapper::pushSpectrum(const SpectrumIdentity& spectrumIdentity, int msLevel, int numPrecursors, int numOverlap)
	{
		DemuxRequestIndex originalIndex;
		originalIndex.msLevel = msLevel;
		originalIndex.spectrumOriginalIndex = spectrumIdentity.index;
		int numDemuxIndices = numPrecursors * numOverlap;
		if (msLevel != 2)
		{
			// spectrum will not be demux'd
			numDemuxIndices = 1;
		}

		for (int demuxIndex = 0; demuxIndex < numDemuxIndices; ++demuxIndex)
		{
			// spectrum will be demux'd
			int pIndex = demuxIndex / numOverlap; // Use floored integer division to repeat the precursor index for the number of overlap sections
			originalIndex.precursorIndex = pIndex;
			originalIndex.demuxIndex = demuxIndex;
			indexMap.push_back(originalIndex);
			spectrumIdentities.push_back(spectrumIdentity);
			spectrumIdentities.back().index = spectrumIdentities.size() - 1;
			spectrumIdentities.back().id += " demux=" + to_string(demuxIndex);
		}
	}

	string SpectrumList_Demux::IndexMapper::injectScanId(string id, size_t scanNumber)
	{
		boost::char_separator<char> sep(" ");
		ScanIdTokenizer tokenizer(id, sep);
		string newId = "";
		for (ScanIdTokenizer::const_iterator token = tokenizer.begin(); token != tokenizer.end(); ++token)
		{
			vector<string> attrs;
			boost::split(attrs, *token, boost::is_any_of("="));
			if (attrs.size() != 2)
			{
				newId += *token + " ";
				continue;
			}
			if (attrs[0] == "scan")
			{
				newId += "scan=" + to_string(scanNumber) + " ";
				newId += "originalScan=" + attrs[1] + " ";
				continue;
			}
			newId += *token + " ";
		}
		return newId;
	}

	PWIZ_API_DECL const map<SpectrumList_Demux::Params::Optimization, string> SpectrumList_Demux::Params::kOptimizationStrings = {
		{ Optimization::NONE, "none" },
		{ Optimization::OVERLAP_ONLY, "overlap_only" }
	};

	std::string SpectrumList_Demux::Params::optimizationToString(Optimization opt)
	{
		return enumToString<Optimization>(opt, kOptimizationStrings);
	}

	SpectrumList_Demux::Params::Optimization SpectrumList_Demux::Params::stringToOptimization(std::string s)
	{
		return stringToEnum<Optimization>(s, kOptimizationStrings);
	}

	SpectrumList_Demux::SpectrumList_Demux(const SpectrumListPtr& inner, const Params& p) :
		SpectrumListWrapper(inner),
		demuxSolver_(new NNLSSolver(p.nnlsMaxIter, p.nnlsEps)),
		lastSolved_(new PreviousDemuxSolution),
		params_(p)
#ifdef _USE_DEMUX_DEBUG_WRITER		
		,
		debugWriter_(boost::make_shared<DemuxDebugWriter>("DemuxDebugOutput.log"))
#endif
	{
#ifdef _PROFILE_PERFORMANCE
		auto t1 = high_resolution_clock::now();
#endif
		switch (params_.optimization)
		{
		case Params::Optimization::NONE:
			pmc_ = boost::make_shared<PrecursorMaskCodec>(inner, params_.variableFill);
			demux_ = boost::make_shared<MSXDemultiplexer>(GenerateMSXParams(params_));
			break;
		case Params::Optimization::OVERLAP_ONLY:
			pmc_ = boost::make_shared<PrecursorMaskCodec>(inner, params_.variableFill);
			demux_ = boost::make_shared<OverlapDemultiplexer>();
			break;
		default: break;
		}
#ifdef _PROFILE_PERFORMANCE
		// add function to be timed here
		auto t2 = high_resolution_clock::now();
		auto duration = duration_cast<microseconds >(t2 - t1).count();
		cout << "Build PrecursorMaskCodec (ReadDemuxScheme()): " << duration << endl;
#endif

#ifdef _PROFILE_PERFORMANCE
		t1 = high_resolution_clock::now();
#endif
		// Generate the IndexMapper using the chosen IPrecursorMaskCodec
		indexMapper_ = boost::make_shared<IndexMapper>(inner, *pmc_);
#ifdef _PROFILE_PERFORMANCE
		// add function to be timed here
		t2 = high_resolution_clock::now();
		duration = duration_cast<microseconds >(t2 - t1).count();
		cout << "Build IndexMapper: " << duration << endl;
#endif
		// Use a SpectrumListCache since we expect to request the same spectra multiple times to extract all demux spectra before moving to the next
		sl_ = boost::make_shared<SpectrumListCache>(inner, MemoryMRUCacheMode_MetaDataAndBinaryData, 200);
		// Record the processing method that will be used to demultiplex
		ProcessingMethod method = pmc_->GetProcessingMethod();
		method.order = static_cast<int>(dp_->processingMethods.size());
		dp_->processingMethods.push_back(method);
		// TODO Sanity-check the user's choice of demultiplexer based on the PrecursorMaskCodec's initial read-through of the data set
		// Initialize the unique methods for demultiplexing
		demux_->Initialize(sl_, pmc_);
	}

	PWIZ_API_DECL SpectrumPtr SpectrumList_Demux::spectrum(size_t index, bool getBinaryData) const
	{
#ifdef _PROFILE_PERFORMANCE
		cout << endl; // add newline to not overlap with normal output
#endif
		// TODO -- make this work for getBinaryData is false
		const IndexMapper::DemuxRequestIndex& demuxRequest = indexMapper_->indexMap[index];
		if (demuxRequest.msLevel != 2)
		{
			// This is an MS1 spectrum, so just return it
			Spectrum_const_ptr originalSpectrum = sl_->spectrum(demuxRequest.spectrumOriginalIndex, true);
			SpectrumPtr newSpectrum = boost::make_shared<Spectrum>(*originalSpectrum);
			newSpectrum->index = index;
			newSpectrum->id = spectrumIdentity(index).id;
			return newSpectrum;
		}
		return boost::make_shared<Spectrum>(*GetDemuxSpectrum(index));
	}

	PWIZ_API_DECL SpectrumPtr SpectrumList_Demux::spectrum(size_t index, DetailLevel detailLevel) const
	{
		// TODO: add ability to deal with non-binary-data requests
		return spectrum(index, true);
	}

	PWIZ_API_DECL size_t SpectrumList_Demux::size() const
	{
		return indexMapper_->indexMap.size();
	}

	PWIZ_API_DECL const SpectrumIdentity& SpectrumList_Demux::spectrumIdentity(size_t index) const
	{
		return indexMapper_->spectrumIdentities.at(index);
	}

	Spectrum_const_ptr SpectrumList_Demux::GetDemuxSpectrum(size_t index) const
	{
		IndexMapper::DemuxRequestIndex& request = indexMapper_->indexMap[index];
		Spectrum_const_ptr refSpectrum = sl_->spectrum(request.spectrumOriginalIndex, true); // The multiplexed spectrum to be demultiplexed
		MatrixPtr solution;
		if (lastSolved_->solution && lastSolved_->origSpecIndex == request.spectrumOriginalIndex)
		{
			// This spectrum has been already solved (there will be separate requests for each precursor of a single spectrum)
			solution = lastSolved_->solution;
		}
		else
		{
#ifdef _PROFILE_PERFORMANCE
			auto t1 = high_resolution_clock::now();
#endif
			// Figure out which spectra to include in the system of equations to demux
			vector<size_t> muxIndices;
			demux_->GetMatrixBlockIndices(request.spectrumOriginalIndex, muxIndices, params_.demuxBlockExtra);
#ifdef _PROFILE_PERFORMANCE
			// add function to be timed here
			auto t2 = high_resolution_clock::now();
			auto duration = duration_cast<microseconds >(t2 - t1).count();
			cout << "GetMatrixBlockIndices: " << duration << endl;
#endif

#ifdef _PROFILE_PERFORMANCE
			t1 = high_resolution_clock::now();
#endif
			// Generate matrices for least squares solve
			MatrixPtr masks;
			MatrixPtr signal;
			demux_->BuildDeconvBlock(request.spectrumOriginalIndex, muxIndices, masks, signal);
#ifdef _PROFILE_PERFORMANCE
			// add function to be timed here
			t2 = high_resolution_clock::now();
			duration = duration_cast<microseconds >(t2 - t1).count();
			cout << "BuildDeconvBlock: " << duration << endl;
#endif

#ifdef _PROFILE_PERFORMANCE
			t1 = high_resolution_clock::now();
#endif
			// Perform the least squares solve
			solution.reset(new MatrixType(masks->cols(), signal->cols()));
			demuxSolver_->Solve(masks, signal, solution);
			lastSolved_->solution = solution;
			lastSolved_->origSpecIndex = request.spectrumOriginalIndex;
#ifdef _PROFILE_PERFORMANCE
			// add function to be timed here
			t2 = high_resolution_clock::now();
			duration = duration_cast<microseconds >(t2 - t1).count();
			cout << "Solve: " << duration << endl;
#endif

#ifdef _USE_DEMUX_DEBUG_WRITER
			if (debugWriter_->IsOpen())
			{
				debugWriter_->WriteDeconvBlock(request.spectrumOriginalIndex, masks, solution, signal);
			}
#endif
		}
		
		// Build a new demultiplexed spectrum from a copy of the original spectrum
		SpectrumPtr demuxed = boost::make_shared<Spectrum>(*refSpectrum);
		demuxed->precursors.clear();
		vector<size_t> deconvIndices;
		pmc_->SpectrumToIndices(refSpectrum, deconvIndices);

		// Make the demux window boundaries and add to the spectrum
		auto demuxIsolationWindow = pmc_->GetIsolationWindow(deconvIndices[request.demuxIndex]);
		
		const auto& originalPrecursor = refSpectrum->precursors[request.precursorIndex];
		auto originalWindow = IsolationWindow(originalPrecursor);
		auto demuxPrecursor = originalPrecursor;
		
		{
			// Rewrite the isolation window based on the boundaries found by the IPrecursorMaskCodec.
			auto lowMz = demuxIsolationWindow.lowMz;
			auto highMz = demuxIsolationWindow.highMz;
			auto offsetMz = (highMz - lowMz) / 2.0;
			auto targetMz = lowMz + offsetMz;
			auto mzUnits = demuxPrecursor.isolationWindow.cvParam(MS_isolation_window_target_m_z).units;
			demuxPrecursor.isolationWindow.set(MS_isolation_window_target_m_z, targetMz, mzUnits);
			demuxPrecursor.isolationWindow.set(MS_isolation_window_lower_offset, offsetMz, mzUnits);
			demuxPrecursor.isolationWindow.set(MS_isolation_window_upper_offset, offsetMz, mzUnits);
		}
		demuxed->precursors.push_back(demuxPrecursor);

		// Add the new spectrum index
		demuxed->index = index;

		// Add the new spectrum identity
		demuxed->id = spectrumIdentity(index).id;

		// Build the new mz and intensity arrays
		demuxed->binaryDataArrayPtrs.clear();
		demuxed->setMZIntensityArrays(vector<double>(), vector<double>(), MS_number_of_detector_counts);
		vector<double>& newMzs = demuxed->getMZArray()->data;
		vector<double>& newIntensities = demuxed->getIntensityArray()->data;
		vector<double>& originalMzs = refSpectrum->getMZArray()->data;
		vector<double>& originalIntensities = refSpectrum->getIntensityArray()->data;

		auto& referenceDemuxIndices = demux_->SpectrumIndices();
		auto summedIntensities = solution->row(referenceDemuxIndices[0]).eval(); // eval() performs copy instead of reference
		for (size_t i = 1; i < referenceDemuxIndices.size(); ++i)
		{
			summedIntensities += solution->row(referenceDemuxIndices[i]);
		}
		auto rawSolutionIntensities = solution->row(referenceDemuxIndices[request.demuxIndex]);
		for (int i = 0; i < rawSolutionIntensities.size(); ++i)
		{
			if (rawSolutionIntensities[i] <= 0.0) continue;
			newMzs.push_back(originalMzs[i]);
			if (!params_.variableFill)
			{
				auto newIntensity = originalIntensities[i] * rawSolutionIntensities[i] / summedIntensities[i];
				newIntensities.push_back(newIntensity);
			}
			else
			{
				newIntensities.push_back(rawSolutionIntensities[i]);
			}
		}
		demuxed->defaultArrayLength = newMzs.size();
		return demuxed;
	}
} // namespace analysis
} // namespace pwiz