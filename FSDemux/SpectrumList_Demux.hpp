//
// SpectrumList_Demux.hpp
//
// Original author: Jarrett Egertson <jegertso .@. uw.edu>
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
//

#ifndef _SPECTRUMLIST_DEMUX_HPP
#define _SPECTRUMLIST_DEMUX_HPP

#include "DemuxSolver.hpp"
#include "IPrecursorMaskCodec.hpp"
#include "IDemultiplexer.hpp"
#include <SpectrumListWrapper.hpp>
#ifdef _USE_DEMUX_DEBUG_WRITER
#include "DemuxDebugWriter.hpp"
#include "DemuxDebugReader.hpp"
#endif
#include "DemuxHelpers.hpp"
#include "DemuxTypes.hpp"

namespace pwiz {
namespace analysis {

	using namespace DemuxTypes;

	/// SpectrumList decorator implementation that can demultiplex spectra of several precursor windows acquired in the same scan.
	/** 
	 * SpectrumList_Demux can separate multiplexed spectra into several demultiplexed spectra by inferring from adjacent multiplexed spectra. This method
	 * can handle variable fill times, requiring that the user specify whether the fill times have varied.
	*/
	class PWIZ_API_DECL SpectrumList_Demux : public msdata::SpectrumListWrapper
	{
	public:

		/// User-defined options for demultiplexing
		struct Params
		{
			/// Optimization methods available
			enum class Optimization
			{
				NONE,
				OVERLAP_ONLY
			};

			/// Key-value pairs for transforming Optimization enums to strings and vice-versa
			static const std::map<Optimization, std::string> kOptimizationStrings;

			/// Converts an optimization enum to a string
			static std::string optimizationToString(Optimization opt);

			/// Converts a string to an optimization enum (returns NONE enum if no enum matches the string)
			static Optimization stringToOptimization(std::string s);

			Params() :
				ppmError(10.0),
				demuxBlockExtra(0.0),
				nnlsMaxIter(50),
				nnlsEps(1e-10),
				applyWeighting(true),
				regularizeSums(true),
				variableFill(false),
				optimization(Optimization::NONE)

			{}
			
			/// Part-per-million error for extracting MS/MS peaks
			double ppmError;
			
			/// Multiplier to expand or reduce the # of spectra considered when demux'ing.
			/// If 0, a fully determined system of equation is built. If > 1.0, the number
			/// of rows included in the system is extended demuxBlockExtra * (# scans in 1 duty cycle)
			
			double demuxBlockExtra;
			
			/// Maximum iterations for NNLS solve
			int nnlsMaxIter;
			
			/// Epsilon value for convergence criterion of NNLS solver
			double nnlsEps;
			
			/// Weight the spectra nearby to the input spectrum more heavily in the solve
			/// than the outer ones
			bool applyWeighting;
			
			/// After demux solve, scale the sum of the intensities contributed form each
			/// of the input windows to match the non-demux'd intensity
			bool regularizeSums;
			
			/// Set to true if fill times are allowed to vary for each scan window
			bool variableFill;
			
			/// Optimizations can be chosen when experimental design is known
			Optimization optimization;
		};

		/// Simple caching object for storing the last solved demultiplexing spectrum
		struct PreviousDemuxSolution
		{
			size_t origSpecIndex; ///< index of original spectrum
			MatrixPtr solution; ///< solution matrix output of DemuxSolver
		};

		/// Generates an abstract SpectrumList_Demux decorator from inner SpectrumList
		/// @param inner The inner SpectrumList
		/// @param p User-defined options
		SpectrumList_Demux(const msdata::SpectrumListPtr& inner, const Params& p = Params());
		
		/// Virtual destructor to allow for th
		virtual ~SpectrumList_Demux() {}

		/// \name SpectrumList Interface
		///@{

		msdata::SpectrumPtr spectrum(size_t index, bool getBinaryData = false) const override;
		msdata::SpectrumPtr spectrum(size_t index, msdata::DetailLevel detailLevel) const override;
		size_t size() const override;
		const msdata::SpectrumIdentity& spectrumIdentity(size_t index) const override;
		///@}

	private:
		/// Container that maps from the indices of the demultiplexed output spectra to their source multiplexed input spectra. This provides
		/// the number of output demultiplexed spectra so that they can be iterated through. This also provides the multiplexed source spectra
		/// for each demultiplexed spectra and an index to the specific demultiplexed spectrum within the source spectrum.
		struct IndexMapper
		{
			/// Shared pointer definition
			typedef boost::shared_ptr<IndexMapper> ptr;
			
			/// Constant shared pointer definition
			typedef boost::shared_ptr<const IndexMapper> const_ptr;

			/// Structure stored by the IndexMapper that is used to uniquely query (or construct) a demultiplexed spectrum
			typedef struct
			{
				int msLevel; ///< Number of rounds of sequential MS performed
				
				size_t spectrumOriginalIndex; ///< Index in SpectrumList of the mux'd spectrum to demux
				
				size_t precursorIndex; ///< Index of the precursor isolation window used to assign a demultiplex window to the multiplexed precursor
				///< isolation window that it was derived from.
				
				size_t demuxIndex; ///< Arbitrarily assigned index of the demultiplexed window used to uniquely map from the original set of
				///< multiplexed spectrum indices to the larger set of indices created by demultiplexing. This could just as well be an unordered
				///<  hash since the precursors are isolated at effectively the same time.
			} DemuxRequestIndex;

			/// SpectrumList of mux spectra to map to
			boost::shared_ptr<const msdata::SpectrumList> original;
			
			/// List of SpectrumIdentities to be assigned ono-to-one to each demux spectrum by their index
			std::vector<msdata::SpectrumIdentity> spectrumIdentities;
			
			/// Maps from the index of a demux spectrum to the index of the mux spectrum it was derived from and the precursor within that spectrum
			/// that it was derived from
			std::vector<DemuxRequestIndex> indexMap;
			
			/// The detail level needed for a non-indeterminate result
			msdata::DetailLevel detailLevel;

			/// Constructs an empty IndexMapper that can be filled with spectra from the given SpectrumListPtr
			explicit IndexMapper(msdata::SpectrumListPtr original, const IPrecursorMaskCodec& pmc);
			
			/// Adds a spectrum to the index map
			/// @param spectrumIdentity SpectrumIdentity to be added as corresponding to the demux'd spectrum (or mux'd spectrum if not MS2)
			/// @param msLevel Number of rounds of tandem MS
			/// @param numPrecursors Number of precursor isolation windows in the spectrum
			/// @param numOverlap Number of overlap windows per precursor
			void pushSpectrum(const msdata::SpectrumIdentity& spectrumIdentity, int msLevel, int numPrecursors, int numOverlap);
			
			/// Parses a list of key-value pairs coded in a string id to replace the old mux index with the new demux index.
			/// This modifes the "scan" key to use the new demux scan index while also reassigning the original scan index to the key "originalScan".
			/// @param[in] id Original scan id to be modified
			/// @param[in] scanNumber The index of the demux spectrum to be added
			/// @return The modified scan id
			static std::string injectScanId(std::string id, size_t scanNumber);
		};

		/// Retrieves demultiplexed spectrum from cache or, if it isn't cached, this delegates the demultiplexing of the spectrum.
		/// @param[in] index Index of the requested demux spectrum (not the same index as the original multiplexed spectrum)
		/// @return The requested demultiplexed spectrum
		msdata::Spectrum_const_ptr GetDemuxSpectrum(size_t index) const;

		/// PrecursorMaskCodec is used for interpreting a series of spectra and generating an MSX design matrix
		IPrecursorMaskCodec::ptr pmc_;

		/// SpectrumList that caches recently used spectra since we expect to access the same spectra multiple times while demultiplexing
		msdata::SpectrumListPtr sl_;

		/// Each input mux'd spectrum is split into multiple demux'd spectra. Therefore, we need to reinterpret spectrum
		/// requests by index and map them to a larger set of indices. The indexMap keeps track of mapping of an input
		/// query index to demux'd sub-spectrum.
		IndexMapper::ptr indexMapper_;

		/// Once a design matrix is generated from the experimental design and a response matrix is made from the signals in the multiplexed spectra, the
		/// demultiplexing problem can be framed as a non-negative least squares problem. This NNLS problem is delegated to the DemuxSolver.
		DemuxSolver::ptr demuxSolver_;

		/// This caches the last solution generated by the DemuxSolver.
		/// We expect to access the demultiplixed spectra in order but may access a multiplexed spectrum multiple times before moving to the next solve.
		/// The primary application is currently to write spectra sequentially to file so caching optimization for random access isn't necessary at this time.
		boost::shared_ptr<PreviousDemuxSolution> lastSolved_;

		/// The demultiplexer to use for generating the matrices to be solved
		IDemultiplexer::ptr demux_;

		/// A set of user-defined options
		Params params_;

#ifdef _USE_DEMUX_DEBUG_WRITER
		
		boost::shared_ptr<DemuxDebugWriter> debugWriter_;
#endif
	};

	typedef SpectrumList_Demux::Params::Optimization DemuxOptimization;

} // namespace analysis
} // namespace pwiz

#endif // _SPECTRUMLIST_DEMUX_HPP