@echo off
setlocal

REM Go to directory that batch file is located and run doxygen there
cd %~dp0
doxygen Doxyfile 1> doxygen-build.log 2> doxygen-build-errors.log