@echo off
setlocal

REM Go to directory that batch file is located and run there
cd %~dp0

REM copy over dlls causing the linker errors
if not exist x64\Release mkdir x64\Release
if not exist x64\Debug mkdir x64\Debug

copy /y pwiz\pwiz_aux\msrc\utility\vendor_api\Waters\vc12_x64\MassLynxRaw.dll x64\Release\MassLynxRaw.dll 1> dll_fix.log 2> dll_fix-errors.log
if %errorlevel% neq 0 exit /b 2
copy /y pwiz\pwiz_aux\msrc\utility\vendor_api\Waters\vc12_x64\cdt.dll x64\Release\cdt.dll 1>> dll_fix.log 2>> dll_fix-errors.log
if %errorlevel% neq 0 exit /b 2
copy /y pwiz\build-nt-x86\pwiz\utility\bindings\CLI\4dbe5b68eb814b65594b6669eb213ae2\baf2sql_c.dll x64\Release\baf2sql_c.dll 1>> dll_fix.log 2>> dll_fix-errors.log
if %errorlevel% neq 0 exit /b 2
copy /y pwiz\build-nt-x86\pwiz\utility\bindings\CLI\4dbe5b68eb814b65594b6669eb213ae2\baf2sql_c.dll x64\Debug\baf2sql_c.dll 1>> dll_fix.log 2>> dll_fix-errors.log
if %errorlevel% neq 0 exit /b 2