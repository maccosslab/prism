PRISM
=====

A full spectrum demultiplexer written in C++

Getting Started
---------------

### Prerequisites

-   Windows 7 or higher build environment

-   Visual C++ 2013 and Visual Studio 2013

-   tar and bz2 extractor, such as [7-zip](http://www.7-zip.org/download.html)

-   doxygen (with doxygen command available in your Windows path)

-   MSFileReader (see below for details)

### MSFileReader

1.  [Register with
    Thermo](https://thermo.flexnetoperations.com/control/thmo/RegisterMemberToAccount)
    and download MSFileReader 3.0

2.  Once you are registered, you can download MSFileReader 3.0 from the Thermo
    web site. It is currently under "Utility Software", and is called
    "MSFileReader 3.0 SP2" (may well have changed by the time you read this,
    though)

3.  Unzip it

4.  Right click on the installer and "Run as Administrator"

5.  Install it accepting defaults

6.  If given the option, install both 32-bit and 64-bit versions.

Installing
----------

Follow the installation steps below in order.

### ProteoWizard

If you have 7-zip (64 bit) installed, just run install.bat (this will build the
debug version) and skip to the final step. Otherwise follow all steps below:

1.  First, extract pwiz-src-3\_9707.tar.bz2 to directory directory pwiz

2.  Copy SpectrumWorkerThreads.cpp to FSDemux\\pwiz\\pwiz\\data\\msdata

3.  Copy SpectrumList\_Thermo.cpp to
    FSDemux\\pwiz\\pwiz\\data\\vendor\_readers\\Thermo

4.  Run b64.bat to build the release version of proteowizard (this will take a
    while to run!)

5.  Run b64\_debug.bat if you want to be able to debug FSDemux

### PRISM

FSDemux.sln can be used to build with Visual Studio 2013. Make sure to set build
configuration to x64. Win32 builds require additional configuration of pwiz.

Usage
-----

See output from

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
prism.exe --help
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Testing
-------

Test data can be downloaded and unpacked by running the script
Tests/get-test-data.sh. This requires that you have Git Bash or another bash
shell installed.

To build Tests you must install gtest:

1.  Install NuGet VS 2013 VSIX (v2.12.0) from
    [nuget.org](https://dist.nuget.org/index.html)

2.  After installing NuGet open the PRISM solution in Visual Studio

3.  Right click on the Tests project in Visual Studio and choose "Manage NuGet
    Packages…"

4.  Search for "gtest" and install "Google Test" (Id: gtest), "FIX8 Gtest
    dependency", and "FIX8 Gtest dependency Symbols". They should be the first,
    second, and third results, respectively.

Alternatively, you could build googletest from source and include it in the
project.

To run tests, choose Menu -\> RESHARPER -\> Unit Tests -\> Run All Unit Tests
from Solution.

Contributing
------------

1.  Fork it!

2.  Create your feature branch: `git checkout -b my-new-feature`

3.  Commit your changes: `git commit -am 'Add some feature'`

4.  Push to the branch: `git push origin my-new-feature`

5.  Submit a pull request

Authors
-------

-   **Jarrett Egertson** - *Initial work*

-   **Austin Keller**
