@echo off

REM clean up
if exist pwiz rmdir pwiz /s /q
if exist pwiz-src-3_0_9706.tar del pwiz-src-3_0_9706.tar

REM extract
"C:\Program Files\7-Zip\7z.exe" e pwiz-src-3_0_9706.tar.bz2
"C:\Program Files\7-Zip\7z.exe" x pwiz-src-3_0_9706.tar -opwiz

REM clean up
del pwiz-src-3_0_9706.tar

REM copy over ProteoWizard modified files
set thermo_path=pwiz\pwiz\data\vendor_readers\Thermo
copy SpectrumList_Thermo.cpp %thermo_path%\SpectrumList_Thermo.cpp
copy SpectrumList_Thermo.hpp %thermo_path%\SpectrumList_Thermo.hpp
copy SpectrumWorkerThreads.cpp pwiz\pwiz\data\msdata\SpectrumWorkerThreads.cpp

REM build pwiz release
cmd /C b64.bat

REM archive the build log
copy pwiz\build64.log pwiz_build64.log

REM build pwiz debug
cmd /C b64_debug.bat

REM archive the debug build log
copy pwiz\build64.log pwiz_build64_debug.log