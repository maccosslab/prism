#!/bin/bash

curl -O https://svn.code.sf.net/p/proteowizard/code/trunk/pwiz/pwiz_tools/Skyline/TestA/Results/MsxTest.zip
unzip -u MsxTest.zip MsxTest.mzML OverlapTest.mzML -d MsxTest

# Clean up
rm MsxTest.zip